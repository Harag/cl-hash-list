(in-package :cl-hash-list)

;;Could have done an ra-list in a CL list but using a struct we can
;;do type tests etc
(defstruct hash-list
  size
  trees)

(defun root-tree (hash-list)
  (and hash-list
       (if (hash-list-p hash-list)
	   (car (hash-list-trees hash-list))
	   (car hash-list))))

(defun next-tree (hash-list)
  (and hash-list
       (if (hash-list-p hash-list)
	   (car (cdr (hash-list-trees hash-list)))
	   (car (cdr hash-list)))))

(defstruct node
  size
  left-child
  right-child
  index
  value
  key
  hash
  max
  min)

;;checking the size or children -- faster vs safer?
(defun leaf-p (node)
  (or (not (node-left-child node))
      (not (node-right-child node))))

(defparameter *hash-function* 'sxhash "Function used to hash key values, 
defaults to sxhash which falls appart on larger values.")

(defun make-hash (value)
  ;;You can trade speed for space by using mod here
  ;;(mod val 10000)
  (funcall *hash-function* value))

(defun hash-cons* (value index key hash left-child right-child)
  (make-node :size (+ 1
		      (node-size left-child)
		      (node-size right-child))
	     :index index
	     :key key
	     :value value
	     :hash hash
	     
	     :max (max hash
		       (node-max left-child)
		       (node-max right-child))
	     :min (min hash
		       (node-min left-child)
		       (node-min right-child))
	     :left-child left-child
	     :right-child right-child))


(defun hash-push-append (value index key hash hash-list)
  ;;cons should return new list*
  (make-hash-list
   :size index
   :trees
   (append (list
	    (make-node :size 1
		       :index index
		       :key key
		       :value value		       
		       :hash hash
		       :max hash
		       :min hash))
	   (if (hash-list-p hash-list)
	       (hash-list-trees hash-list)
		hash-list))))

(defun next-index (hash-list)
  (if hash-list
    (if (hash-list-size hash-list)
	(incf (hash-list-size hash-list))
	0)
    0))

(defun hash-cons (value hash-list &key key-function)
  ;;(break "~A" hash-list)
  (let* ((root (root-tree hash-list))
	(next-tree (next-tree hash-list))
	(key (if key-function
			     (funcall key-function value)
			     value))
	(hash (make-hash key))
	(next-index (next-index hash-list)))

    ;;(break "fux ~A" next-index)
    (if next-tree
	(if  (= (node-size root)
		(node-size next-tree))	   
	     (let ((rest (cddr (hash-list-trees hash-list)))
		   (new-node (hash-cons* value next-index key hash root next-tree)))
	       ;;cons should return new list*               
	       (make-hash-list
		:size next-index
		:trees
		(if rest
		    (append (list new-node) rest)
		    (list new-node))))		   
	     (hash-push-append value next-index key hash hash-list))
	(hash-push-append value next-index key hash hash-list))))

(defun node-lookup (size node index)
  (cond ((not (node-left-child node))
	 node)
	((= index 0)
	 node)
	(t
	 (let ((size-split (/ size 2)))
	   (if (<= index size-split)
		   (node-lookup size-split (node-left-child node) (- index 1))
		   (node-lookup size-split (node-right-child node)
				(- index 1 (truncate size-split))))))))

(defun hash-lookup (hash-list index)
  (let ((node (if (listp hash-list)
		  (car hash-list)
		  (car (hash-list-trees hash-list)))))    
    (if (< index (node-size node))
	(node-lookup (node-size node) node index)
	(hash-lookup (cdr (if (listp hash-list)
				 hash-list
				 (hash-list-trees hash-list)))
			(- index (node-size node))))))

;;Used to see how much tree access is done.
(defparameter *search-node-access-count* 0)

(defun node-map (function node)
  (if (leaf-p node)
      (funcall function node)

      (progn
	(if node
	    (funcall function node))

	(node-map function (node-left-child node))
	(node-map function (node-right-child node)))))

(defun hash-map (function hash-list)
  (when hash-list
    (let ((node (if (hash-list-p hash-list)
		    (car (hash-list-trees hash-list))
		    (car hash-list))))
      (node-map function node)
      (hash-map function (cdr (if (hash-list-p hash-list)
				 (hash-list-trees hash-list)
				 hash-list))))))

(defun hash-map-value (function hash-list)
;;  (break "~A" function)
  (hash-map (lambda (node)
;;	      (break "fuck ~a" (list(node-value node)))
	      (funcall function (list (node-value node))))
	    hash-list))

(defun hash-map-key-value (function hash-list)
  (hash-map (lambda (node)
	      (funcall function (node-key node) (node-value node)))
	    hash-list))

(defun hash-list-values (hash-list)
  (let ((values))
    (hash-map (lambda (node)
		     (push (node-value node) values))
	      hash-list)
    values))

(defun hash-list-keys (hash-list)
  (let ((keys))
    (hash-map (lambda (node)
		     (push (node-key node) keys))
	      hash-list)
    keys))

(defun node-search (node value &key (test 'equal))
  (incf *search-node-access-count*)
  (when node
    (if (leaf-p node)
	(if (funcall test (node-value node) value )
	    node)
	(or
	 (if (funcall test (node-value node) value )
	     node)        
	 (node-search (node-left-child node) value)
	 (node-search (node-right-child node) value)))))

(defun value-search (hash-list value &key (test 'equal))
  (when hash-list
      (let ((node (if (listp hash-list)
		      (car hash-list)
		      (car (hash-list-trees hash-list)))))
	(values
	 (or (node-search node value :test test)
	     (value-search (cdr (if (hash-list-p hash-list)
				      (hash-list-trees hash-list)
				      hash-list))
			     value))
	 *search-node-access-count*))))

(defun node-hash-search (node value hash-value)  
  (incf *search-node-access-count*)
  (when node
 
    (if (leaf-p node)
	(progn
	  ;;(break "~A ~A ~%~A" value hash-value node)
	  (if (equalp (node-hash node) hash-value)
	      (progn
		;;(break "fuck ~A" node)
		node)))	
	(or
	 (if (equalp (node-hash node) hash-value)
	    node)
         (if (and (<= hash-value
		      (node-max (node-left-child node)))
		  (>= hash-value
		     (node-min (node-left-child node))))
	     (node-hash-search (node-left-child node) value hash-value))
	 (if (and
	      (<= hash-value
		  (node-max (node-right-child node)))
	      (>= hash-value
		     (node-min (node-right-child node))))
	     (node-hash-search (node-right-child node) value hash-value))))))

(defun hash-search (hash-list value)
  (let ((val (make-hash value)))
    (when hash-list
      (let ((node (if (hash-list-p hash-list)
		      (car (hash-list-trees hash-list))
		      (car hash-list)
		      )))

	#|
	(if (> *search-node-access-count* 99999 )
	     (break "To many lookups is happening for this value ~A ~%~A" value val)
	    )
	|#
	(values
	 (or (node-hash-search node value val)
	     (hash-search (cdr (if (listp hash-list)
				      hash-list
				      (hash-list-trees hash-list)))
			  value))
	 
	 *search-node-access-count*)))))

