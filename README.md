# cl-hash-list

## What is it
Random access lists that can be access with key/hash as well as an index.

So what does a random access list do for you?

http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.55.5156&rep=rep1&type=pdf

In the above paper you fill find a short discussion on min lists, which inspired me to
combine ra list with min and max properties based on a hash which basically gives us a
list and a hash table.

## Other implementations of ra list

There is an existing implementation (https://github.com/VincentToups/random-access-lists) of random access lists in CL but I wanted to understand the guts of the algorithm better and abuse it to serve as hash tables, naturaly instead of implmenting hash tables on it like https://github.com/VincentToups/persistent-tables

The mentioned implementation is based on https://srfi.schemers.org/srfi-101/srfi-101.sls

The scheme implimentation did not implement the ra list cleanly either and adjusted the underlying structure to have less trees (uses more storage) as seen here https://www.cs.oberlin.edu/~jwalker/ra-list/.

## Performance

As far as lookup/search the speed between sbcl hashtable and cl-hash-list they are very close ONLY for data is sorted by the key/hash you are using when loading it into the list.. If the data is unsorted you are going to take end up visiting between 10% and 40% of the nodes in the tree.

The cl-hash-list is about 10% smaller than sbcl hashtables in memory if you use hashing for the min max.

Loading the cl-hash-list list takes double the time it does to load the SBCL hashtable.


## Thoughts

As far as a list/tree goes I think it performs very well but it needs the dice to fall in its favour to compare to hash tables for day to day use.

I will implement a sort for it *some-day* to see what kind of penalty that incurs, it might save it for use as a database if the data is not updated constantly. There is already a node visit counter and that could be checked periodically and a sort could be kicked of automatically when the data becomes to fragmented. But then again there are no gains to its use just hacks that will eventually make it unwieldly.

Implmenting a hash table over it with buckets could also be an option, at least then you would only have to sort individual buckets if they become fragmented/unsorted.

I have run out of time and hope for it and need to move on to other stuff.


## Implementation

Its bare basic just implemented cons, map and search/lookup functionality.

I needed these basics to test if it could replace the use of hashtables in cl-naive-store, especially indexing because SBCL hash tables crashes the lisp image when you load millions of them... ;). Just cutting down the number of hash tables by using lists and hash table combination sorted out cl-naive-store indexing.


## To try it

If you have less than 12 gigs of ram allocated to sbcl reduce the number of test
records from 100000000

Running any of the tests the first time (30 secs for 100mil) will take time because its building the test db after that it will just reuse the *DB*

;;Normal search
(cl-hash-list::test-value-search 100000000 0)

;;Fast Search
(cl-hash-list::test-hash-search 100000000 0)

See test.lisp for other usage examples.

